import React, { Component } from 'react';
import AlertComponent from './alertComponent';
import Dropzone from 'react-dropzone';
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import axios from 'axios';

let dropzoneRef;
class PostFormView extends Component {

    constructor() {
        super();
        this.state = {
            description: '',
            selectedFile: '',
        };

        this.postInformation = this.postInformation.bind(this);
    }

    onDrop(files) {
        this.setState({
            files
        });
    }

    postInformation(evt) {


        const reader = new FileReader();
        reader.onload = () => {
            const fileAsBinaryString = reader.result;
            console.log(fileAsBinaryString);
            const data = new FormData();
            data.append('filename', this.state.files[0].name);
            data.append('image', fileAsBinaryString);

            let formData = new FormData();

            formData.append('description', this.state.files[0].name);
            formData.append('selectedFile', fileAsBinaryString);
            axios.post('riotrauma.kinghost.net:21316/upload', formData)
                .then((result) => {
                    // access results...
                });

            /*fetch('http://localhost:5000/upload', {
                method: 'POST',
                mode:'cors',
                headers: new Headers({
                    'Content-Type': 'application/json'
                }),
                body: {image: fileAsBinaryString},
            }).then((response) => {
                response.json().then((body) => {
                    console.log(body);
                });
            });*/
        };
        if (this.state.files[0]) {
            reader.readAsDataURL(this.state.files[0]);
        }
    }

    onChange = (e) => {
        const state = this.state;

        switch (e.target.name) {
            case 'selectedFile':
                state.selectedFile = e.target.files[0];
                break;
            default:
                state[e.target.name] = e.target.value;
        }

        this.setState(state);
    }

    onSubmit = (e) => {
        e.preventDefault();
        const { description, selectedFile } = this.state;
        let formData = new FormData();

        formData.append('description', description);
        formData.append('selectedFile', selectedFile);

        axios.post('riotrauma.kinghost.net:21316/upload', formData)
            .then((result) => {
                // access results...
            });
    }

    render() {
        return (

            <form onSubmit={this.onSubmit}>
                <input
                    type="text"
                    name="description"
                    value={this.state.description}
                    onChange={this.onChange}
                />
                <input
                    type="file"
                    name="selectedFile"
                    onChange={this.onChange}
                />
                <button type="submit">Submit</button>
            </form>
        );
    }
}

export default PostFormView;