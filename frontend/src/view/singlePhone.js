import React, { Component } from 'react';
import { Media, Jumbotron } from 'reactstrap';

class SinglePhone extends Component {

    render() {
        return (
            <Media>
                <Media body>
                    <div>
                        <Jumbotron>
                            <img src={this.props.data.photo} alt={this.props.data.model} />
                            <h3>
                                {`${this.props.data.brand}  -  ${this.props.data.model}`}
                            </h3>

                            <h6>{`Code: ${this.props.data.code}`}</h6>
                            <h6>{`Price: R$ ${this.props.data.price}`}</h6>
                        </Jumbotron>
                    </div>
                </Media>
            </Media>
        );
    }
}

export default SinglePhone;