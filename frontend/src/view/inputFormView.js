import React, { Component } from 'react';
import SinglePhone from './singlePhone';
import AlertComponent from './alertComponent';

import { Col, Button, Form, FormGroup, Label, Input, InputGroupAddon } from 'reactstrap';



class InputFormView extends Component {

    constructor() {
        super();
        this.state = {
            code: "",
            backendResult: [],
            backendError: []
        };

        this.handleCodeChange = this.handleCodeChange.bind(this);
        this.getInformation = this.getInformation.bind(this)
    }

    handleCodeChange(evt) {
        this.setState({ code: evt.target.value });
    }


    addChild() {
        // State change will cause component re-render
        this.setState(this.state.concat([
            { id: 2, name: "Another Name" }
        ]))
    }

    getInformation(evt) {
        let newMobile = {
            code: this.state.code
        };
        let header = new Headers({
            'Access-Control-Allow-Origin': '*',
            'Content-Type': 'application/json'
        });
        let url = `http://localhost:5000/claro/mobile/${newMobile.code}`; 
        console.log(url);
        fetch(url, {
            method: 'GET',
            mode: "cors",
            headers: header,
        }).then((response) => {
            response.json().then((body) => {
                let i = 0;
                var smartPhones = [];
                console.log(body.json);
                if (body.result) {
                    body.json.forEach(element => {
                        smartPhones.push({ key: i++, data: element });
                    });
                    this.setState({ backendResult: smartPhones });
                }
                else {
                    this.setState({ backendError:[{key: 0, result: body.result, msg: body.json }] });
                }
            });
        });
    }

    render() {
        return (

            <Form>
                {
                    this.state.backendError.map((item) => (
                        <AlertComponent key={item.key} result={item.result} msg={item.msg} />
                    ))
                }

                <FormGroup row>
                    <Col>
                        <Label >Insert on Database</Label >
                        <Label >(Please enter all information)</Label >
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Col xs="6" sm="4">
                        <InputGroupAddon addonType="prepend">Smartphone Code</InputGroupAddon>
                        <Input placeholder="(optional)" onChange={this.handleCodeChange} />
                    </Col>
                </FormGroup>
                <Button onClick={this.getInformation}>Submit</Button>
                {
                    this.state.backendResult.map((item) => (
                        <SinglePhone key={item.key} data={item.data} />
                    ))
                }
            </Form>
        );
    }
}

export default InputFormView;