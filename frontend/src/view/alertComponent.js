import React, { Component } from 'react';
import { Alert } from 'reactstrap';

class AlertComponent extends Component {

    render() {
        return (
            <Alert color={this.props.result? "success":"danger"}>
                {this.props.msg}
          </Alert>
        );
    }
}

export default AlertComponent;