import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import PostFormView from './view/postFormView';
import InputFormView from './view/inputFormView';
import { Col,  Nav, NavItem, TabContent, TabPane, NavLink, CardBody, Card } from 'reactstrap';
import classnames from 'classnames';

class App extends Component {

  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      activeTab: '1'
    };
  }

  toggle(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      });
    }
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
        <Col xs="6" sm="12" md={{ size: 8, offset: 2 }}>
          <Nav tabs>
            <NavItem>
              <NavLink
                className={classnames({ active: this.state.activeTab === '1' })}
                onClick={() => { this.toggle('1'); }}>
                Include Mobile on Backend
            </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                className={classnames({ active: this.state.activeTab === '2' })}
                onClick={() => { this.toggle('2'); }}>
                Search for Mobiles
            </NavLink>
            </NavItem>
          </Nav>

          <TabContent activeTab={this.state.activeTab}>
            <TabPane tabId="1">
              <Card>
                <CardBody>
                  <PostFormView />
                </CardBody>
              </Card>
            </TabPane>
            <TabPane tabId="2">
              <Card>
                <CardBody>
                  <InputFormView />
                </CardBody>
              </Card>
            </TabPane>
          </TabContent>
        </Col>
        </div>
        );
      }
    }
    
    export default App;
