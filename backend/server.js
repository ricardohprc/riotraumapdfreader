const express = require('express');
var multer = require('multer');
const bodyParser = require('body-parser');
const uuidv4 = require('uuid/v4');
var cors = require('cors');
var fs = require('fs');
const path = require('path');
const unzip = require('unzip');
var lib = require("pdfreader");
var http = require('http');
var PdfReader = lib.PdfReader;
var Rule = lib.Rule;

const app = express();
const port = process.env.PORT || 5000;
app.use(cors());

// configure storage
const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, './uploads');
    },
    filename: (req, file, cb) => {
        const newFilename = `${uuidv4()}${path.extname(file.originalname)}`;
        cb(null, newFilename);
    },
});
// create the multer instance that will be used to upload/save the file
const upload = multer({ storage });
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));


function printRawItems(filePath, callback) {
    new PdfReader().parseFileItems(filePath, function (err, item) {
        if (!item || err) {
            callback();
        }
        else if (item.page) {
            // end of file, or page
            globalList.push(printRows());
            rows = {}; // clear rows for next page
        }
        else if (item.text) {
            // accumulate text items into rows object, per line
            (rows[item.y] = rows[item.y] || []).push(item.text);
        }
    });
}

function extractFields(pdfyList) {
    let output = [];
    let tmpObject = { name: "", competencia: "", unidade: "", email: "" };
    let i = 0;
    pdfyList.forEach(function (pdf) {
        i = 0;
        pdf.forEach(function (element) {
            if (element === NOME_PARSER) {
                tmpObject.name = pdf[i - 1];
                tmpObject.competencia = pdf[i - 2];
            }
            else if (element === UNIDADE_PARSER) {
                tmpObject.unidade = pdf[i - 1];
            }
            else if (element === EMAIL_PARSER) {
                tmpObject.email = pdf[i - 1];
            }
            i++;
        });
        output.push(tmpObject);
        tmpObject = { name: "", competencia: "", unidade: "", email: "" };
    });
    return output;
}

function printResult(msg) {
    pdfAsText += msg;
    console.log(msg);
}

app.post('/upload', upload.single('selectedFile'), function (req, res, next) {
    // req.file is the `avatar` file
    // req.body will hold the text fields, if there were any
    let folder = "./uploads/";
    let filename = req.file.filename;
    let extractedFolderName = folder + filename.replace('.zip', '');
    console.log(filename);
    console.log(extractedFolderName);
    fs.createReadStream(folder + filename).pipe(
        unzip.Extract({ path: extractedFolderName })).on('close', function () {
            fs.readdir(extractedFolderName, (err, files) => {
                files.forEach(file => {
                    console.log(extractedFolderName + "/" + file);
                    printRawItems(extractedFolderName + "/" + file, function () {
                        let result = extractFields(file);
                        console.log(extractedFolderName + "/" + file);
                        console.log(result);
                        res.send({ result: true, json: `upload/${output}` });
                    });
                });
            })
        });
});

app.get('/',function(req,res){
       
    res.sendFile('index.html');

});

var multer = require('multer');
const bodyParser = require('body-parser');
const uuidv4 = require('uuid/v4');
var cors = require('cors');
var fs = require('fs');
const path = require('path');
const unzip = require('unzip');
var lib = require("pdfreader");
var PdfReader = lib.PdfReader;
var Rule = lib.Rule;
const port = process.env.PORT || 21316;

var express = require('express')
  , app = express();
 

app.use(cors());

// configure storage
const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, './uploads');
    },
    filename: (req, file, cb) => {
        const newFilename = `${uuidv4()}${path.extname(file.originalname)}`;
        cb(null, newFilename);
    },
});
// create the multer instance that will be used to upload/save the file
const upload = multer({ storage });
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));


function printRawItems(filePath, callback) {
    new PdfReader().parseFileItems(filePath, function (err, item) {
        if (!item || err) {
            callback();
        }
        else if (item.page) {
            // end of file, or page
            globalList.push(printRows());
            rows = {}; // clear rows for next page
        }
        else if (item.text) {
            // accumulate text items into rows object, per line
            (rows[item.y] = rows[item.y] || []).push(item.text);
        }
    });
}

function extractFields(pdfyList) {
    let output = [];
    let tmpObject = { name: "", competencia: "", unidade: "", email: "" };
    let i = 0;
    pdfyList.forEach(function (pdf) {
        i = 0;
        pdf.forEach(function (element) {
            if (element === NOME_PARSER) {
                tmpObject.name = pdf[i - 1];
                tmpObject.competencia = pdf[i - 2];
            }
            else if (element === UNIDADE_PARSER) {
                tmpObject.unidade = pdf[i - 1];
            }
            else if (element === EMAIL_PARSER) {
                tmpObject.email = pdf[i - 1];
            }
            i++;
        });
        output.push(tmpObject);
        tmpObject = { name: "", competencia: "", unidade: "", email: "" };
    });
    return output;
}

function printResult(msg) {
    pdfAsText += msg;
    console.log(msg);
}

app.post('/upload', upload.single('selectedFile'), function (req, res, next) {
    // req.file is the `avatar` file
    // req.body will hold the text fields, if there were any
    let folder = "./uploads/";
    let filename = req.file.filename;
    let extractedFolderName = folder + filename.replace('.zip', '');
    console.log(filename);
    console.log(extractedFolderName);
    fs.createReadStream(folder + filename).pipe(
        unzip.Extract({ path: extractedFolderName })).on('close', function () {
            fs.readdir(extractedFolderName, (err, files) => {
                files.forEach(file => {
                    console.log(extractedFolderName + "/" + file);
                    printRawItems(extractedFolderName + "/" + file, function () {
                        let result = extractFields(file);
                        console.log(extractedFolderName + "/" + file);
                        console.log(result);
                        res.send({ result: true, json: `upload/${output}` });
                    });
                });
            })
        });
});

app.get('/', function(req, res){
  res.send('Utilizando Express no nodejs');
});
 
app.listen(port, () => console.log(`Listening on port ${port}`));